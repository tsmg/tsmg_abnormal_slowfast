import os
import sys
import json
import argparse
import traceback
import glog as logger

from app.utils import db
from app.utils import utils
from app.utils.processing import SafetyProcessing

from slowfast.config.defaults import get_cfg
import slowfast.utils.checkpoint as cu

FLAGS = None
config = utils.load_config()


def main(camera_id, camera_config, slowfast_cfg):
    safety_processing = SafetyProcessing.get_instance(camera_id, camera_config, slowfast_cfg)
    safety_processing.run()

def parse_args():
    parser = argparse.ArgumentParser(
        description="Provide SlowFast video training, testing, and demo pipeline."
    )
    parser.add_argument(
        "--gpu",
        help="Number of gpus",
        default=1,
        type=int,
    )
    parser.add_argument(
        "--camera_id",
        help="camera id",
        default="7",
        type=str,
    )

    if len(sys.argv) == 1:
        parser.print_help()
    return parser.parse_args()

def load_config(args):
    # Setup cfg.
    cfg = get_cfg()

    # Load config from cfg.
    cfg.merge_from_file(config['SLOWFAST_RECOGNITION']['config_AVA'])

    # Inherit parameters from args.
    if hasattr(args, "num_shards") and hasattr(args, "shard_id"):
        cfg.NUM_SHARDS = args.num_shards
        cfg.SHARD_ID = args.shard_id
    if hasattr(args, "rng_seed"):
        cfg.RNG_SEED = args.rng_seed
    if hasattr(args, "output_dir"):
        cfg.OUTPUT_DIR = args.output_dir

    return cfg

if __name__ == '__main__':

    args = parse_args()
    slowfast_cfg = load_config(args)

    cam_id = int(args.camera_id)

    camera_config_str = db.get_camera_config(cam_id)

    if camera_config_str is None:
        logger.info('Can not found config for camera id %d' % cam_id)
        sys.exit(0)

    camera_config = json.loads(camera_config_str)

    os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)

    logger.info('============================================================')
    logger.info('Starting tracking adrress id: %d, camera id: %d' % (config['BASE']['ADDRESS_ID'], cam_id))
    logger.info('CAM URL: %s' % camera_config['URL'])
    logger.info('[APP] Environment:   %s' % config['APP']['ENV'])
    logger.info('[APP] Debug:         %s' % config['APP']['DEBUG'])
    logger.info('============================================================')

    main(cam_id, camera_config, slowfast_cfg)
