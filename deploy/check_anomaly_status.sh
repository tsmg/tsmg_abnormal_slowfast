#!/bin/bash
PATH=$PATH:/usr/local/bin
export PATH

source /home/ubuntu/release/py36/bin/activate
cd /home/ubuntu/release/projects/tsmg_abnormal_engine/tsmg_abnormal_slowfast/deploy

nohup python -u check_anomaly_status.py --camera_ids 7 --gpus 0 > ../logs/check_anomaly_status.log 2>&1 &
