#!/bin/bash
PATH=$PATH:/usr/local/bin
export PATH

source /home/ubuntu/release/py35/bin/activate
cd /home/ubuntu/release/projects/tsmg_abnormal_engine/tsmg_abnormal_slowfast

nohup python -u produce_tracking_for_demo.py > ./logs/produce_tracking_for_demo.log 2>&1 &
