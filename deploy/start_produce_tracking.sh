#!/bin/bash
PATH=$PATH:/usr/local/bin
export PATH

source /home/ubuntu/release/py36/bin/activate
cd /home/ubuntu/release/projects/tsmg_abnormal_engine/tsmg_abnormal_slowfast

nohup python -u produce_tracking.py > ./logs/produce_tracking.log 2>&1 &
