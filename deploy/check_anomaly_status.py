import subprocess
import traceback
import glog as logger
import argparse

START_CAMERA_CMD = 'bash /home/ubuntu/release/projects/tsmg_abnormal_engine/tsmg_abnormal_slowfast/deploy/start_anomaly_camera.sh %d %d'
START_PRODUCE_TRACKING = 'bash /home/ubuntu/release/projects/tsmg_abnormal_engine/tsmg_abnormal_slowfast/deploy/start_produce_tracking.sh'

def is_camera_running(cam_id):
    s = subprocess.Popen(('ps ax | grep ' + '"python -u main.py --camera_id %d"' % int(cam_id) + ' | grep -v grep | wc -l'), shell = True, stdout = subprocess.PIPE)
    for x in s.stdout:
        if int(x) > 0:
            return True
        else:
            return False
    return False

def is_produce_tracking_running():
    s = subprocess.Popen(('ps ax | grep ' + '"python -u produce_tracking.py" | grep -v grep | wc -l'), shell = True, stdout = subprocess.PIPE)
    for x in s.stdout:
        if int(x) > 0:
            return True
        else:
            return False
    return False

if __name__ == '__main__':
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    parser.add_argument('--camera_ids', nargs='?', type=str, required=True, default='7,8', help='[Option] Camera IDS')
    parser.add_argument('--gpus', nargs='?', type=str, required=True, default='0,0', help='[Option] Camera GPUS')
    args = parser.parse_args()

    logger.info('--------------------Start Checking-----------------------')

    for cam_id, gpu in zip(args.camera_ids.split(','), args.gpus.split(',')):
        try:
            logger.info('Checking process of camera id %d .....................' % int(cam_id))
            if is_camera_running(cam_id):
                logger.info('Running.')
            else:
                logger.info('Stopped.')
                subprocess.Popen((START_CAMERA_CMD % (int(cam_id), int(gpu))), shell = True, stdout = subprocess.PIPE)
                logger.info('Started.')
        except Exception as e:
            logger.info('Exception: {}'.format(e))
            logger.info('Traceback: {}'.format(str(traceback.format_exc())))

    try:
        logger.info('Checking process of produce tracking .........................')
        if is_produce_tracking_running():
            logger.info('Produce tracking is Running.')
        else:
            logger.info('Produce tracking is Stopped.')
            subprocess.Popen((START_PRODUCE_TRACKING), shell=True, stdout=subprocess.PIPE)
            logger.info('Produce tracking producer is Started.')
    except Exception as e:
        logger.info('Exception: {}'.format(e))
        logger.info('Traceback: {}'.format(str(traceback.format_exc())))
