#!/bin/bash
PATH=$PATH:/usr/local/bin
export PATH

source /home/ubuntu/release/py36/bin/activate
cd /home/ubuntu/release/projects/tsmg_abnormal_engine/tsmg_abnormal_slowfast

nohup python -u main.py --camera_id $1 --gpu $2 > ./logs/cam_$1.log 2>&1 &
