import numpy as np
import slowfast.utils.checkpoint as cu
import slowfast.utils.distributed as du
from slowfast.utils import logging
from slowfast.utils import misc
from slowfast.datasets import cv2_transform
from slowfast.models import build as model_builder
from slowfast.datasets.cv2_transform import scale
import slowfast.utils.checkpoint as cu
import slowfast.utils.multiprocessing as mpu
# from slowfast.config.infer_config import get_config
import torch

class SlowFastRecognition:
    # Here will be the instance stored.
    __instance = None

    @staticmethod
    def get_instance(cfg):
        """ Static access method. """
        if SlowFastRecognition.__instance == None:
            print('Initialize SlowFastRecognition ... ')
            SlowFastRecognition(cfg)
        return SlowFastRecognition.__instance

    def __init__(self, cfg):
        if SlowFastRecognition.__instance != None:
            raise Exception('This class is a singleton!')
        else:
            SlowFastRecognition.__instance = self

        self.cfg = cfg
        # Set random seed from configs.
        np.random.seed(self.cfg.RNG_SEED)
        torch.manual_seed(self.cfg.RNG_SEED)

        self.SlowFastRecognition = self.load_model(self.cfg)

    def load_model(self, cfg):
        # Build the video model and print model statistics.
        model = model_builder.build_model(cfg)
        model.eval()
        # misc.log_model_info(model)

        # Load a checkpoint to test if applicable.
        if self.cfg.TEST.CHECKPOINT_FILE_PATH != "":
            ckpt = self.cfg.TEST.CHECKPOINT_FILE_PATH
        elif cu.has_checkpoint(self.cfg.OUTPUT_DIR):
            ckpt = cu.get_last_checkpoint(self.cfg.OUTPUT_DIR)
        elif self.cfg.TRAIN.CHECKPOINT_FILE_PATH != "":
            ckpt = self.cfg.TRAIN.CHECKPOINT_FILE_PATH
        else:
            raise NotImplementedError("Unknown way to load checkpoint.")

        cu.load_checkpoint(
            ckpt,
            model,
            self.cfg.NUM_GPUS > 1,
            None,
            inflation=False,
            convert_from_caffe2= "caffe2" in [self.cfg.TEST.CHECKPOINT_TYPE, self.cfg.TRAIN.CHECKPOINT_TYPE],
        )

        return model

    def predict(self, inputs, boxes):
        outputs = self.SlowFastRecognition(inputs, boxes)

        return outputs.cpu().detach().numpy()
