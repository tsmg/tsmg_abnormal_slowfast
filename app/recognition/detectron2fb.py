import numpy as np
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
import slowfast.utils.checkpoint as cu

class Detectron2FB:
    # Here will be the instance stored.
    __instance = None

    @staticmethod
    def get_instance(cfg):
        """ Static access method. """
        if Detectron2FB.__instance == None:
            print('Initialize Detectron2FB ... ')
            Detectron2FB(cfg)
        return Detectron2FB.__instance

    def __init__(self, cfg):
        if Detectron2FB.__instance != None:
            raise Exception('This class is a singleton!')
        else:
            Detectron2FB.__instance = self

        self.cfg = cfg
        self.detectron2fb = self.load_model()

    def load_model(self):
        
        dtron2_cfg_file = self.cfg.DEMO.DETECTRON2_OBJECT_DETECTION_MODEL_CFG
        dtron2_cfg = get_cfg()
        dtron2_cfg.merge_from_file(model_zoo.get_config_file(dtron2_cfg_file))
        dtron2_cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = .5
        dtron2_cfg.MODEL.WEIGHTS = self.cfg.DEMO.DETECTRON2_OBJECT_DETECTION_MODEL_WEIGHTS
        object_predictor = DefaultPredictor(dtron2_cfg)

        return object_predictor

    def predict(self, image):
        outputs = self.detectron2fb(image)

        return outputs
