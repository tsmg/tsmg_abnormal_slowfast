import numpy as np
from joblib import dump, load

class SvmClassification:
    # Here will be the instance stored.
    __instance = None

    @staticmethod
    def get_instance(weights):
        """ Static access method. """
        if SvmClassification.__instance == None:
            print('Initialize SvmClassification ... ')
            SvmClassification(weights)
        return SvmClassification.__instance

    def __init__(self, weights):
        if SvmClassification.__instance != None:
            raise Exception('This class is a singleton!')
        else:
            SvmClassification.__instance = self

        self.svm_classification = self.load_model(weights)

    def load_model(self, model_path):
        svm_clf = load(model_path)

        return svm_clf

    def predict(self, scores):
        if len(scores.shape) != 2:
            outputs = []
        else:
            outputs = self.svm_classification.predict(scores)

        for i in outputs:
            if int(i) != 0:
                return int(i)

        return 0
