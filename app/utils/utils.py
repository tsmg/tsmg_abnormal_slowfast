import cv2
import json
import numpy as np

from math import exp
from random import randrange

import matplotlib.path as mplPath
import glog as logger


def load_config(config_path='./configs/config.json'):
    with open(config_path) as json_file:
        return json.load(json_file)


def write_jpeg_4_live(redis_client, frame, key, turbojpeg):
    encoded = turbojpeg.encode(frame)

    # Store encoded data in Redis
    redis_client.set(key, encoded)


def read_jpeg_4_live(redis_client, key):
    encoded = redis_client.get(key)
    return encoded


def get_camera_config(config, camera_id):
    camera_configs = config.CAMERAS
    for c in camera_configs:
        if c['ID'] == camera_id:
            return c
    return None


def split_image_types(str_images):
    object_images = []
    proof_images = str_images.split(',')

    if len(proof_images) > 1:
        object_images.append(proof_images[1])
        del proof_images[1]

    str_object_images = ','.join(object_images)
    str_proof_images = ','.join(proof_images)
    return str_object_images, str_proof_images

def soft_uniform_sampling(input_feat, raw_pred, raw_uncertainty, param):
    (interval, min_cnt, max_cnt) = param
    local_samples = interval * 2
    labeled_index = list()

    threshold = np.sort(raw_pred)[::-1][int(0.3 * len(raw_pred))]
    for i in range(len(raw_pred)):
        if raw_pred[i] >= threshold:
            labeled_index.append(i)

    if len(labeled_index) / 2.0 < min_cnt:
        pos = np.argsort(raw_pred)[0: min_cnt].tolist()
        neg = np.argsort(raw_pred)[-1: -min_cnt].tolist()
        labeled_index = list(set(pos + neg))
    elif len(labeled_index) / 2.0 > max_cnt:
        cut_begin = randrange(len(labeled_index) - 2 * max_cnt)
        assert (len(labeled_index) >= cut_begin + max_cnt * 2)
        labeled_index = labeled_index[cut_begin: cut_begin + max_cnt * 2]

    labeled_index.sort()
    sample_index = set()
    for i in labeled_index:
        b = int(i - local_samples / 2)
        e = int(i + 1 + local_samples / 2)
        for j in range(b, e):
            if 0 <= j < len(raw_pred):
                sample_index.add(j)
    sample_index = list(sample_index)
    sample_index.sort()
    labeled_index_in_the_graph = []
    for i in range(len(sample_index)):
        set_labeled_index = set(labeled_index)
        if sample_index[i] in set_labeled_index:
            labeled_index_in_the_graph.append(i)
    output_dimension = len(sample_index)
    # establish the adjacent matrix A^tilde

    adj = np.zeros((output_dimension, output_dimension))
    for i in range(output_dimension):
        for j in range(output_dimension):
            adj[i][j] = exp(-abs(sample_index[i] - sample_index[j]))
    # compute the degree matrix D^tilde
    d = np.zeros_like(adj)
    for i in range(output_dimension):
        for j in range(output_dimension):
            d[i][i] += adj[i][j]
    # calculate the normalized adjacency A^hat
    d_inv_sqrt = np.power(d, -0.5)
    d_inv_sqrt[np.isinf(d_inv_sqrt)] = 0
    adj_hat = np.dot(np.dot(d_inv_sqrt, adj), d_inv_sqrt)

    # obtain the features of samples
    output_feat = np.zeros((output_dimension, input_feat.shape[-1]))
    for i in range(output_dimension):
        output_feat[i] = input_feat[sample_index[i]]
    return output_feat.astype(np.float32), adj_hat.astype(np.float32), \
           np.array(labeled_index_in_the_graph), np.array(labeled_index)

#======Polygon============
def get_box_center(bbox):
    x1, y1, x2, y2 = bbox
    center_x = int((x1 + x2) / 2)
    center_y = int((y1 + y2) / 2)

    return center_x, center_y

def moving_camera(box_cam):
    print("Move camera", box_cam)

def is_inside_polygon(pt, poly):
    bbPath = mplPath.Path(poly)
    return bbPath.contains_point(pt)

def convert_8p_to_poly(box_8point):
    return [[box_8point[0],box_8point[1]],[box_8point[2],box_8point[3]],
            [box_8point[4],box_8point[5]],[box_8point[6],box_8point[7]]]

def slide_windows(inputSize, slide_pixel = 100):
    width = inputSize[0]
    height = inputSize[1]

    base_windows = [540, 960]
    time_x = (width - base_windows[1]) / slide_pixel
    time_y = (height - base_windows[0]) / slide_pixel

    if time_x.is_integer() == False:
        time_x = int(time_x) + 1
    if time_y.is_integer() == False:
        time_y = int(time_y) + 1

    arr_xmin = []
    for x in range(time_x + 1):
        xmin = x * slide_pixel
        arr_xmin.append(xmin)
    for i,x in enumerate(arr_xmin):
        if (x + base_windows[1]) > width:
            arr_xmin[i] = width - base_windows[1]

    arr_ymin = []
    for y in range(time_y + 1):
        ymin = y * slide_pixel
        arr_ymin.append(ymin)
    for i,y in enumerate(arr_ymin):
        if (y + base_windows[0]) > height:
            arr_ymin[i] = height - base_windows[0]

    arr_final_windows = []
    for x in arr_xmin:
        xmin = x
        xmax = xmin + base_windows[1]
        for y in arr_ymin:
            ymin = y
            ymax = ymin + base_windows[0]
            arr_final_windows.append([xmin, ymin, xmax, ymax])

    return arr_final_windows

def rule_base_sidewalk(list_box_sidewalk, box_human):
    list_data_out = []
    for box_sidewalk in list_box_sidewalk:
        box_sidewalk = convert_8p_to_poly(box_sidewalk)
        count_human = 0
        list_box_wrong = []
        for box in box_human:
            #1: Tinh center point
            center_point = get_box_center(box)
            #2: Check inside polygon
            is_inside_area = is_inside_polygon(center_point, box_sidewalk)
            #3: Neu nam trong thi xu ly logic
            if is_inside_area:
                count_human += 1
                list_box_wrong.append(box)
        if count_human >= 10:
            list_data_out.append((box_sidewalk, list_box_wrong))

    return list_data_out

def rule_base_fighting(list_box_sidewalk, box_human, label_ids):
    list_true_box = []
    #Step1: Loc cac box co class la 33,34,63,70
    if len(box_human) != len(label_ids):
        return False

    for box_sidewalk in list_box_sidewalk:
        box_sidewalk = convert_8p_to_poly(box_sidewalk)
        for idx, box in enumerate(box_human):
            #1: Tinh center point
            center_point = get_box_center(box)
            #2: Check inside polygon
            is_inside_area = is_inside_polygon(center_point, box_sidewalk)
            #3: Neu nam trong thi xu ly logic
            #if is_inside_area:
            for lb in label_ids[idx]:
                if lb in [33, 34, 63, 70]:
                    logger.info("Fighting RuleBase Detected")
                    return True
    return False

def rule_base_roadaccident(list_box_road, box_human, label_ids):
    list_true_box = []
    #Step1: Loc cac box co class la 11(Stand)
    if len(box_human) != len(label_ids):
        return []

    for idx, box in enumerate(box_human):
        for lb in label_ids[idx]:
            if int(lb) == 11:
                list_true_box.append(box)
                break

    #Step2: Kiem tra xem co trong vung can khoanh khong
    list_data_out = []
    for box_road in list_box_road:
        box_road = convert_8p_to_poly(box_road)
        count_human = 0
        list_box_wrong = []
        for box in box_human:
            #1: Tinh center point
            center_point = get_box_center(box)
            #2: Check inside polygon
            is_inside_area = is_inside_polygon(center_point, box_road)
            #3: Neu nam trong thi xu ly logic
            if is_inside_area:
                count_human += 1
                list_box_wrong.append(box)
        if count_human >= 20:
            list_data_out.append((box_road, list_box_wrong))

    return list_data_out
