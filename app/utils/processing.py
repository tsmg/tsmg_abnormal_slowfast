import os, sys
# from time import time
from time import sleep

import numpy as np
import pandas as pd
import cv2
import torch
import math
import argparse

import slowfast.utils.checkpoint as cu
import slowfast.utils.distributed as du
from slowfast.utils import logging
from slowfast.utils import misc
from slowfast.datasets import cv2_transform
from slowfast.models import build as model_builder
from slowfast.datasets.cv2_transform import scale
import slowfast.utils.checkpoint as cu
import slowfast.utils.multiprocessing as mpu

from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg

from joblib import dump, load

from app.utils import utils
from app.utils import db
import json

from datetime import datetime
import shutil
from app.utils import kafka_utils

from app.recognition.slowfast_recognition import SlowFastRecognition
from app.recognition.svm_classification import SvmClassification
from app.recognition.detectron2fb import Detectron2FB

import queue
import redis
from turbojpeg import TurboJPEG
from threading import Thread
import glog as logger

class SafetyProcessing:
    # Here will be the instance stored.
    __instance = None

    @staticmethod
    def get_instance(camera_id, camera_config, sf_config):
        """ Static access method. """
        if SafetyProcessing.__instance == None:
            print('Loading Safety Processing ... ')
            SafetyProcessing(camera_id, camera_config, sf_config)
        return SafetyProcessing.__instance

    def __init__(self, camera_id, camera_config, sf_config):
        if SafetyProcessing.__instance != None:
            raise Exception('This class is a singleton!')
        else:
            SafetyProcessing.__instance = self

        self.camera_id = camera_id
        self.camera_config = camera_config
        self.config = utils.load_config()
        self.slowfast_config = sf_config
        self.svm_config = self.config["SVM_CLASSIFICATION"]
        self.kafka_config = self.config["KAFKA_LOCAL"]

        # self.frames = queue.Queue()
        self.org_frames = queue.Queue()
        self.mid_frame = None
        # self.pred_labels = queue.Queue()

        self.vid = cv2.VideoCapture(camera_config["URL"])
        #self.vid = cv2.VideoCapture('/home/ubuntu/release/projects/tsmg_abnormal_engine/tsmg_abnormal_slowfast/test_video/pasteur_01_bangchung.mp4')
        self.cam_cfg_url = camera_config["URL"]
        self.video_fps = int(self.vid.get(cv2.CAP_PROP_FPS))
        self.video_size = (int(self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)), int(self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)))

        self.slowfast_recognition = SlowFastRecognition.get_instance(self.slowfast_config)
        self.svm_classification = SvmClassification.get_instance(self.svm_config["MODEL_PATH"])
        self.detectron2fb = Detectron2FB.get_instance(self.slowfast_config)

        self.producer = kafka_utils.connect_kafka_producer(self.kafka_config['BOOTSTRAP_SERVERS'])
        self.redis_client = redis.Redis(host=self.config['APP']['REDIS_SERVER']['HOST'], port=self.config['APP']['REDIS_SERVER']['PORT'],
                                   db=0)
        self.turbojpeg = TurboJPEG('/usr/lib/x86_64-linux-gnu/libturbojpeg.so')
        self.check_safety = False

        # Add new for processing
        self.sf_cfg = sf_config
        self.seq_len = self.sf_cfg.DATA.NUM_FRAMES*self.sf_cfg.DATA.SAMPLING_RATE

    def reconnect_cam(self):
        self.vid.release()
        self.vid = cv2.VideoCapture(self.cam_cfg_url)
        self.clear_queue_data()

    def clear_queue_data(self):
        self.org_frames.queue.clear()

    def detector(self, object_predictor , image, cfg , display_height, display_width ):
        outputs = object_predictor.predict(image)
        fields = outputs["instances"]._fields
        pred_classes = fields["pred_classes"]
        selection_mask = pred_classes == 0
        # acquire person boxes
        pred_classes = pred_classes[selection_mask]
        pred_boxes = fields["pred_boxes"].tensor[selection_mask]
        scores = fields["scores"][selection_mask]
        boxes = cv2_transform.scale_boxes(cfg.DATA.TEST_CROP_SIZE,
                                            pred_boxes,
                                            display_height,
                                            display_width)
        return boxes

    

    def read_frames(self):
        images, success, count_err = [], True, 0
        frames = []
        org_frames = []

        while True:
            try:
                success, frame = self.vid.read()

                if success:
                    self.org_frames.put(frame)
                else:
                    count_err += 1
                    if count_err % 1000 == 0:
                        logger.error("ERROR READ FRAME FROM VIDEO: %s" % count_err)
                        self.reconnect_cam()
                    continue

            except Exception as e:
                logger.error('Exception: {}'.format(e))
                logger.error('Traceback: {}'.format(str(traceback.format_exc())))

    # xu ly dem nguoi tu tap dam dong o day.
    def process_frames(self):
        list_box_sidewalk_8point = self.camera_config['AREA_SIDEWALK']
        list_box_road_8point = self.camera_config['LANES'][0]['AREA_DETECTION']

        #default label
        label_abnormal = 0
        label_ids = []

        start_time, end_time = None, None
        frames, org_frames = [], []
        mid_frame = None

        # Get list slide windows
        arr_windows_slide = utils.slide_windows([self.video_size[0], self.video_size[1]], self.config['SLOWFAST_RECOGNITION']['SLIDE_PIXEL'])

        while True:
            try:
                if start_time is None:
                    start_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

                tmp_org_frame = self.org_frames.get()
                org_frames.append(tmp_org_frame)

                frame_processed = tmp_org_frame#cv2.cvtColor(tmp_org_frame, cv2.COLOR_BGR2RGB)
                frame_processed = scale(self.sf_cfg.DATA.TEST_CROP_SIZE, frame_processed)
                frames.append(frame_processed)

                if len(frames) < self.seq_len:
                    continue

                #predict all person box in all frame
                mid_frame = org_frames[self.seq_len//2]

                # detect box in mid frame
                if self.slowfast_config.DETECTION.ENABLE:
                    boxes = self.detector(self.detectron2fb , mid_frame, self.sf_cfg , self.video_size[1], self.video_size[0])
                    boxes = torch.cat([torch.full((boxes.shape[0], 1), float(0)).cuda(), boxes], axis=1)

                # processing inputs
                inputs = torch.from_numpy(np.array(frames)).float()
                inputs = inputs / 255.0
                # Perform color normalization.
                inputs = inputs - torch.tensor(self.sf_cfg.DATA.MEAN)
                inputs = inputs / torch.tensor(self.sf_cfg.DATA.STD)
                # T H W C -> C T H W.
                inputs = inputs.permute(3, 0, 1, 2)

                # 1 C T H W.
                inputs = inputs.unsqueeze(0)

                # Sample frames for the fast pathway.
                index = torch.linspace(0, inputs.shape[2] - 1, self.sf_cfg.DATA.NUM_FRAMES).long()
                fast_pathway = torch.index_select(inputs, 2, index)

                # Sample frames for the slow pathway.
                index = torch.linspace(0, fast_pathway.shape[2] - 1,
                                        fast_pathway.shape[2]//self.sf_cfg.SLOWFAST.ALPHA).long()
                slow_pathway = torch.index_select(fast_pathway, 2, index)
                inputs = [slow_pathway, fast_pathway]

                # Transfer the data to the current GPU device.
                if isinstance(inputs, (list,)):
                    for i in range(len(inputs)):
                        inputs[i] = inputs[i].cuda(non_blocking=True)
                else:
                    inputs = inputs.cuda(non_blocking=True)

                #predict SlowFast
                if not len(boxes):
                    preds = torch.tensor([])
                else:
                    preds = self.slowfast_recognition.predict(inputs, boxes)
                    preds_ori = preds
                    pred_masks = preds_ori > .1
                    label_ids = [np.nonzero(pred_mask)[0] for pred_mask in pred_masks]

                # lay lai vi tri box theo dung size
                boxes = boxes.cpu().detach().numpy()
                ratio = np.min(
                    [self.video_size[1], self.video_size[0]]
                ) / self.sf_cfg.DATA.TEST_CROP_SIZE
                boxes = boxes[:, 1:] * ratio

                # Khoi tao co xem co bat thuong hay khong
                is_anomaly = False

                if (len(label_ids) > 0):
                    #Check rulebase RoadAccident
                    list_wrong_roadaccident = utils.rule_base_roadaccident(list_box_road_8point, boxes, label_ids)
                    if len(list_wrong_roadaccident) > 0:
                        label_abnormal = 2
                        is_anomaly = True
                        logger.info("RoadAccident Detected!")
                    
                    #Check rulebase Fighting
                    is_fighting = utils.rule_base_fighting(list_box_sidewalk_8point, boxes, label_ids)
                    
                    if is_fighting ==True:
                        label_abnormal = 3
                        is_anomaly = True
                        logger.info("Fighting Detected!")

                #Check rulebase side walk
                list_wrong_sidewalk = utils.rule_base_sidewalk(list_box_sidewalk_8point, boxes)
                if len(list_wrong_sidewalk) > 0:
                    label_abnormal = 7
                    is_anomaly = True
                    logger.info("TuTapDamDong Detected!")

                # Gom box voi scores thanh cap
                tmp_arr_box_with_score = []
                if len(preds.shape) == 2 and len(boxes) > 0:
                    for box, box_pred in zip(boxes.astype(int), preds):
                        tmp_arr_box_with_score.append([box.astype(int), box_pred])

                label_svm = self.svm_classification.predict(preds)

                if int(label_svm) != 0:
                    is_anomaly = True
                    # utils.moving_camera(windows)
                    label_abnormal = 3

                end_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

                label_name = self.config["CLASS_NAME"][str(label_abnormal)]

                if is_anomaly is True:
                    logger.info('Anomaly: ' + label_name)
                    self.save_video(self.camera_id, org_frames, start_time, end_time, label_abnormal)
                else:
                    logger.info('Normal')

                start_time, end_time = None, None
                frames, org_frames = [], []
                #frames = frames[self.seq_len//2 - 1:]
                #org_frames = org_frames[self.seq_len//2 - 1:]
                mid_frame = None
            except Exception as e:
                logger.error('Exception: {}'.format(e))
                logger.error('Traceback: {}'.format(str(traceback.format_exc())))

    def save_video(self, camera_id, frames, start_time, end_time, label):
        if self.is_enough_storage() is False:
            pass

        label_text = self.config["CLASS_NAME"][str(label)]

        str_start_time = start_time.replace(' ','_').replace(':','-')
        str_end_time = end_time.split(' ')[-1].replace(' ','_').replace(':','-')

        output_name = str(self.camera_id) + "_" + str_start_time + "_" + str_end_time + "_" + label_text +'.mp4'
        relative_out_path = self.get_relative_path()
        full_path = os.path.join(self.config["VIDEO_CONFIG"]["VIDEO_ROOT"], relative_out_path)
        if not os.path.exists(full_path):
            os.makedirs(full_path)

        video_path = os.path.join(full_path, output_name)
        video_four_cc = cv2.VideoWriter_fourcc(*'mp4v')
        out_vid = cv2.VideoWriter(video_path, video_four_cc, self.video_fps, self.video_size)
        for i, frame in enumerate(frames):
            out_vid.write(frame)
        out_vid.release()

        proof_path = os.path.join(relative_out_path, output_name)
        obj_data = {'camera_id': self.camera_id, 'safety_id': 1, 'video_proof': proof_path,
                    'start_time': start_time, 'end_time': end_time}

        kafka_utils.publish_message(self.producer, self.kafka_config['SAFETY_TRACKING']['TOPIC_NAME'],
                            self.kafka_config['SAFETY_TRACKING']['TOPIC_PARTITION'],
                            json.dumps(obj_data))

    def get_relative_path(self):
        year = str(datetime.now().year)
        month = "{:02d}".format(datetime.now().month)
        day = "{:02d}".format(datetime.now().day)
        relative_path = os.path.join(self.config['VIDEO_CONFIG']['VIDEO_PROOF'], year, month, day)
        return relative_path

    def is_enough_storage(self):
        total, used, free = shutil.disk_usage(self.config['VIDEO_CONFIG']['VIDEO_ROOT'])
        free = free // (2**30)

        if free < self.config['VIDEO_CONFIG']['FREE_STORAGE']:
            return False

        return True

    def check_safety_time(self):
        check_safety = self.camera_config["CHECK_SAFETIES"]
        for range_time, _ in check_safety.items():
            range_time = [int(t) for t in range_time.split('-')]
            while True:
                if range_time[0] <= datetime.now().hour <= range_time[-1]:
                    self.check_safety = True
                else:
                    self.check_safety = False

                sleep(1)

    def run(self):
        # check_safeties_thread = Thread(target=self.check_safety_time)
        read_frames_thread = Thread(target=self.read_frames)
        process_frames_thread = Thread(target=self.process_frames)

        # check_safeties_thread.start()
        read_frames_thread.start()
        process_frames_thread.start()

        # check_safeties_thread.join()
        read_frames_thread.join()
        process_frames_thread.join()
