import sys
import time

import MySQLdb as _mysql

from app.utils import utils

config = utils.load_config()


def db_connect():
    db = _mysql.connect(host=config['APP']['DB']['MYSQL_HOST'], port=config['APP']['DB']['MYSQL_PORT'],
                        user=config['APP']['DB']['MYSQL_USER'], passwd=config['APP']['DB']['MYSQL_PASSWD'],
                        db=config['APP']['DB']['MYSQL_DB'], charset='utf8')
    return db


def get_camera_config(camera_id):
    db_conn = db_connect()
    db_cursor = db_conn.cursor()

    str_sql = "SELECT content from camera_config WHERE camera_id = '%d'" % camera_id
    db_cursor.execute(str_sql)
    result = db_cursor.fetchone()
    db_conn.close()

    if result is None:
        return None

    return result[0]


def insert_safety_tracking(db_conn, data):
    db_cursor = db_conn.cursor()

    str_sql = "INSERT INTO safety_tracking(camera_id, safety_id, start_time, end_time,\
                video_proof, created) \
               VALUES ('%d', '%d', '%s', '%s', '%s', '%s')" % \
              (data['camera_id'], data['safety_id'], data['start_time'], data['end_time'], data['video_proof'], data['created'])

    db_cursor.execute(str_sql)
    safety_tracking_id = db_cursor.lastrowid

    return safety_tracking_id

def update_safety_tracking(db_conn, data, safety_tracking_id):
    db_cursor = db_conn.cursor()

    str_sql = "UPDATE safety_tracking SET video_proof='%s', end_time='%s', \
                updated='%s' WHERE id = '%d'" % \
              (data['video_proof'], data['end_time'], data['updated'], safety_tracking_id)

    db_cursor.execute(str_sql)

    return safety_tracking_id

def get_safety_tracking(db_conn, camera_id, safety_id):
    db_cursor = db_conn.cursor()

    str_sql = "SELECT id, video_proof, end_time from safety_tracking WHERE camera_id = '%d'\
                AND safety_id = '%d' ORDER BY created DESC" % \
                (camera_id, safety_id)
    db_cursor.execute(str_sql)
    results = db_cursor.fetchone()

    return results
