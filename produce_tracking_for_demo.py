import json
import traceback
import glog as logger
import random
import time

from datetime import datetime
from app.utils import db
from app.utils import utils
from app.utils import kafka_utils

config = utils.load_config()


def process_tracking(db_conn, producer, public_producer, obj_data):
    try:
        last_insert_id = None
        safety_tracking = dict()
        tracking_row = db.get_safety_tracking(db_conn, obj_data['camera_id'], obj_data['safety_id'])
        is_update = False
        if tracking_row is not None:
            start_time = datetime.strptime(obj_data['start_time'], '%Y-%m-%d %H:%M:%S')
            elapsed_time = start_time - tracking_row[2]
            if elapsed_time.total_seconds() < config["SLOWFAST_RECOGNITION"]["TIME_LENGTH"]:
                is_update = False

        safety_tracking['end_time'] = obj_data['end_time']
        if tracking_row is None or is_update is False:
            safety_tracking['camera_id'] = obj_data['camera_id']
            safety_tracking['safety_id'] = obj_data['safety_id']
            safety_tracking['start_time'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            safety_tracking['video_proof'] = obj_data['video_proof']
            safety_tracking['created'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            safety_tracking_id = db.insert_safety_tracking(db_conn, safety_tracking)
            last_insert_id = safety_tracking_id
        else:
            safety_tracking['video_proof'] = tracking_row[1] + "," + obj_data['video_proof']
            safety_tracking['updated'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            safety_tracking_id = db.update_safety_tracking(db_conn, safety_tracking, tracking_row[0])
            last_insert_id = safety_tracking_id

        # Publish tracking to center kafka
        obj_data['engine_tracking_id'] = last_insert_id
        kafka_utils.publish_message(producer, config['KAFKA_CENTER']['SAFETY_TRACKING_PRODUCER']['TOPIC_NAME'],
                                    config['KAFKA_CENTER']['SAFETY_TRACKING_PRODUCER']['TOPIC_PARTITION'],
                                    json.dumps(obj_data))

        # Publish another message to center kafka

        created_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        dummy_obj_data = {"camera_id": obj_data['camera_id'], "type": obj_data['safety_id'],
                          "engine_tracking_id": last_insert_id, "time": created_time}
        kafka_utils.publish_message(public_producer, 'emergency_warning', 0, json.dumps(dummy_obj_data))

        # Commit your changes in the database
        db_conn.commit()
        logger.info(
            "[SAFETY TRACKING] Saved id: %d, camera_id: %d, created: %s" %
            (safety_tracking_id, obj_data['camera_id'], datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    except Exception as e:
        logger.error('Exception: {}'.format(e))
        logger.error('Traceback: {}'.format(str(traceback.format_exc())))

        return False

    return True


if __name__ == '__main__':
    logger.info('Start safety tracking producer ...')

    producer = kafka_utils.connect_kafka_producer(config['KAFKA_CENTER']['BOOTSTRAP_SERVERS'])
    public_producer = kafka_utils.connect_kafka_producer('118.70.56.9:9092')
    db_conn = db.db_connect()

    time_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    address_ids = [15, 17, 28, 29, 44, 500, 501, 508, 509, 859, 863, 878, 881, 1250, 2712, 2713, 2715, 2716, 2718, 2744, 2746, 2747, 2749, 2766, 2767, 2769, 2770, 2794, 2845, 2847, 2848, 2854, 2855, 2857, 2860, 2861, 2863, 2866, 2875, 2889]

    consumer = [{'address_id': 15, 'camera_id': 1, 'safety_id': 3, 'video_proof': 'nkkn_01_bangchung.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 2767, 'camera_id': 8, 'safety_id': 3, 'video_proof': 'pasteur_01_bangchung.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 878, 'camera_id': 7, 'safety_id': 3, 'video_proof': 'pnt_01_bangchung.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 29, 'camera_id': 4, 'safety_id': 3, 'video_proof': 'hbt_04_bangchung.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 15, 'camera_id': 1, 'safety_id': 1, 'video_proof': '1_1_20201128-16-37-20_1_otochay_cut.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 15, 'camera_id': 1, 'safety_id': 2, 'video_proof': '2_1_20201128_1_cut.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 15, 'camera_id': 1, 'safety_id': 2, 'video_proof': '2_1_20201128_2_cut.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 15, 'camera_id': 1, 'safety_id': 7, 'video_proof': '7_1_20201128-16-37-20_1_tutapdongnguoi_cut.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 29, 'camera_id': 4, 'safety_id': 1, 'video_proof': '1_4_20201128-15-52-19_4_xechay_cut.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 29, 'camera_id': 4, 'safety_id': 1, 'video_proof': '1_4_20201128-15-57-19_4_chaycotdien_cut.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 29, 'camera_id': 4, 'safety_id': 1, 'video_proof': '1_4_20201128-16-02-19_4_xechay_cut.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'address_id': 29, 'camera_id': 4, 'safety_id': 7, 'video_proof': '7_4_20201128-15-52-19_4_tutapdongnguoi_cut.mp4',
                 'start_time': time_now, 'end_time': time_now},
                {'camera_id': 4, 'safety_id': 8, 'video_proof': '7_4_20201128-15-52-19_4_tutapdongnguoi_cut.mp4', 'object_id': 1, 'start_time': time_now,
                 'end_time': time_now},
                {'camera_id': 4, 'safety_id': 8, 'video_proof': '7_4_20201128-15-52-19_4_tutapdongnguoi_cut.mp4', 'object_id': 2, 'start_time': time_now,
                 'end_time': time_now},
                {'camera_id': 4, 'safety_id': 8, 'video_proof': '7_1_20201128-16-37-20_1_tutapdongnguoi_cut.mp4', 'object_id': 3, 'start_time': time_now,
                 'end_time': time_now},
                {'camera_id': 4, 'safety_id': 8, 'video_proof': '7_1_20201128-16-37-20_1_tutapdongnguoi_cut.mp4', 'object_id': 4, 'start_time': time_now,
                 'end_time': time_now},
                {'camera_id': 4, 'safety_id': 5, 'video_proof': '7_4_20201128-15-52-19_4_tutapdongnguoi_cut.mp4', 'object_id': 1, 'start_time': time_now,
                 'end_time': time_now},
                {'camera_id': 4, 'safety_id': 5, 'video_proof': '7_1_20201128-16-37-20_1_tutapdongnguoi_cut.mp4', 'object_id': 2, 'start_time': time_now,
                 'end_time': time_now},
                {'camera_id': 4, 'safety_id': 5, 'video_proof': '7_1_20201128-16-37-20_1_tutapdongnguoi_cut.mp4', 'object_id': 3, 'start_time': time_now,
                 'end_time': time_now}
                ]

    num = random.randint(2, 5)
    for address_id, mess in list(zip(random.sample(address_ids, num), random.sample(consumer, num))):
        mess['address_id'] = address_id
        if mess['safety_id'] in [5, 8]:
            mess['score'] = random.uniform(0, 1)
        ret = process_tracking(db_conn, producer, public_producer, mess)
        time.sleep(30)

    producer.close()
    public_producer.close()

    db_conn.close()
