import sys

import json
import time
import traceback
import glog as logger

from datetime import datetime
from app.utils import db
from app.utils import utils
from app.utils import kafka_utils

config = utils.load_config()
db_conn = db.db_connect()


def process_tracking(db_conn, producer, obj_data):
    try:
        last_insert_id = None
        safety_tracking = dict()
        tracking_row = db.get_safety_tracking(db_conn, obj_data['camera_id'], obj_data['safety_id'])
        is_update = False
        if tracking_row is not None:
            start_time = datetime.strptime(obj_data['start_time'], '%Y-%m-%d %H:%M:%S')
            elapsed_time = start_time - tracking_row[2]
            if elapsed_time.total_seconds() < config["SLOWFAST_RECOGNITION"]["TIME_LENGTH"]:
                is_update = True

        safety_tracking['end_time'] = obj_data['end_time']
        if tracking_row is None or is_update is False:
            safety_tracking['camera_id'] = obj_data['camera_id']
            safety_tracking['safety_id'] = obj_data['safety_id']
            safety_tracking['start_time'] = obj_data['start_time']
            safety_tracking['video_proof'] = obj_data['video_proof']
            safety_tracking['created'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            safety_tracking_id = db.insert_safety_tracking(db_conn, safety_tracking)
            last_insert_id = safety_tracking_id
        else:
            safety_tracking['video_proof'] = tracking_row[1] + "," + obj_data['video_proof']
            safety_tracking['updated'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            safety_tracking_id = db.update_safety_tracking(db_conn, safety_tracking, tracking_row[0])
            last_insert_id = safety_tracking_id

        # Publish tracking to center kafka
        obj_data['address_id'] = config['BASE']['ADDRESS_ID']
        obj_data["engine_tracking_id"] = last_insert_id
        kafka_utils.publish_message(producer, config['KAFKA_CENTER']['SAFETY_TRACKING_PRODUCER']['TOPIC_NAME'],
                                    config['KAFKA_CENTER']['SAFETY_TRACKING_PRODUCER']['TOPIC_PARTITION'],
                                    json.dumps(obj_data))

        # Commit your changes in the database
        db_conn.commit()
        logger.info(
            "[SAFETY TRACKING] Saved id: %d, camera_id: %d, created: %s" %
            (safety_tracking_id, obj_data['camera_id'], datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    except Exception as e:
        logger.error('Exception: {}'.format(e))
        logger.error('Traceback: {}'.format(str(traceback.format_exc())))

        return False

    return True

if __name__ == '__main__':
    logger.info('Start safety tracking producer ...')

    while True:
        producer = kafka_utils.connect_kafka_producer(config['KAFKA_CENTER']['BOOTSTRAP_SERVERS'])

        consumer = kafka_utils.consume_messages(
            config['KAFKA_LOCAL']['BOOTSTRAP_SERVERS'],
            config['KAFKA_LOCAL']['SAFETY_TRACKING']['GROUP_NAME'],
            config['KAFKA_LOCAL']['SAFETY_TRACKING']['TOPIC_NAME'],
            config['KAFKA_LOCAL']['SAFETY_TRACKING']['TOPIC_PARTITION'])

        for mess in consumer:
            consumer.commit()
            ret = process_tracking(db_conn, producer, json.loads(mess.value))

            if ret:
                consumer.commit()
            else:
                # Reconnect DB
                db_conn = db.db_connect()

        producer.close()
        consumer.close()
        time.sleep(0.5)

    db_conn.close()
